scaleway
========

Script to manage scaleway development servers.

Functionality
-------------

* show: shows the IP address of the instance.
* mount: sshfs mounts the user's home of the instance into the local user's *mnt*
* umount: sshfs umount.

Roadmap
-------

* Spinning down snapshotting and terminating servers.
* Creating instances from the snapshots.
